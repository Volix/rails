class ArticleController < ApplicationController
	def index
		@count=Article.all.count
		if params[:page]
			page = params[:page].to_i*10
			@articles = Article.order("updated_at DESC").last(page)
		else
			@articles = Article.order("updated_at DESC").last(10)
		end
	end
	def show
		@article=Article.find_by_id
	end
	def new
		@article=Article.new
	end
	def create
		
		@article=Article.new(arguments)
		if @article.save
			redirect_to article_show_path(id: @article.id)
		else
			rednder 'new'
		end
	
	 
	end
	def remove
	@article=Article.find(params[:id])
	@article.destroy
	redirect_to root_url
	end
	def edit
		@article=Article.find(params[:id])
	end
	def update
		@article=Article.find(params[:id])
		if @article.update(arguments)
			redirect_to :action=>:show, id: @article.id
		end	
	end
	private
	 def arguments
	 params.permit(:title, :content, :date)
	 end
	
end
